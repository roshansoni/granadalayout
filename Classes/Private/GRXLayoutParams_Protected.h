#import "GRXLayoutParams.h"

@interface GRXLayoutParams ()

- (void)setView:(UIView *)view;

@end
